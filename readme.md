# TP INFO910 - Devops

L'application réalisée est un petit site qui répertorie des critiques de films, avec deux fonctionnalités simples pour ajouter un film et ajouter des critiques sur un film existant.

D'un point de vue logiciel, l'application est constituée de deux containers, un pour une BDD MySql et un pour le site web (développé en python avec Flask).

## Lancement de l'application

### Avec docker

L'application peut être démarré en locale avec la commande
`docker compose up`
Le site est alors accessible à l'adresse http://localhost:9000/

### Avec Kubernetes

L'application peut être lancée dans un cluster kubernetes avec la commande : 
`kubectl apply -f k8s/`
Bien que les différents noeuds soient correctement initialisés (d'après le dashboard), je n'ai pas réussi à me connecter au service du site exposé dans movies-svc.yml. Je n'ai donc pas pu vérifié le bon fonctionnement de l'application sur le cluster k8s.