from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField, StringField, TextAreaField
from wtforms.validators import DataRequired

class ReviewForm(FlaskForm):
    name = StringField('Nom', [DataRequired()])
    review = TextAreaField('Avis', [DataRequired()])
    rating = IntegerField('Note', [DataRequired()])
    submit = SubmitField("Envoyer mon avis")

class MovieForm(FlaskForm):
    title = StringField('Titre', [DataRequired()])
    director = StringField('Directeur', [DataRequired()])
    year = IntegerField('Année', [DataRequired()])
    submit = SubmitField("Ajouter")
