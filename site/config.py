import os

class Config(object):
    MYSQL_DATABASE_PASSWORD_FILE = "/run/secrets/mysql/password"

    def __init__(self):
        f = open(Config.MYSQL_DATABASE_PASSWORD_FILE, "r")
        pw = f.read()
        f.close()

        self.MYSQL_DATABASE_USER = 'root'
        self.MYSQL_DATABASE_PASSWORD = pw
        self.MYSQL_DATABASE_HOST = 'mysql'
        self.MYSQL_DATABASE_DB = 'movies'
        self.TESTING = False
        self.DEBUG = False
        self.SECRET_KEY = os.urandom(32)
