from datetime import datetime
from flask import Flask
from flask import render_template
from flaskext.mysql import MySQL
from query import *
from data import *
from forms import ReviewForm, MovieForm

from config import Config

app = Flask(__name__)
mysql = MySQL()
queryHelper = MovieQueries(mysql)

app.config.from_object(Config())
mysql.init_app(app)


@app.route('/', methods=['GET','POST'])
def movies():
    form = MovieForm()
    error = None
    if form.validate_on_submit():
        movie = Movie(form.title.data, datetime(year=form.year.data, month=1, day=1), form.director.data)
        error = queryHelper.add_movie(movie)

    movies = queryHelper.get_movies()
    return render_base_template("index.html",
        title="Tous les films",
        length=len(movies),
        movies = movies,
        form = form,
        error = error
    )

@app.route('/reviews/<id>', methods=['GET','POST'])
def reviews(id):
    form = ReviewForm()
    error = None
    if form.validate_on_submit():
        review = Review(id, form.name.data, form.review.data, form.rating.data, datetime.now())
        error = queryHelper.add_review(review)

    reviews = queryHelper.get_reviews(id)
    movie = queryHelper.get_movie(id)
    if(len(movie) != 0):
        movie = movie[0]
    return render_base_template("movie.html",
        title="Avis sur un film",
        form = form,
        reviews = reviews,
        length = len(reviews),
        movie = movie,
        error = error
    )

def render_base_template(page,**content):
    return render_template(page, **content)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
