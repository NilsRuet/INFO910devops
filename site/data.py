class Review():
    def __init__(self, movieId, user, review, rating, date, id = None):
        self.id = id
        self.movieId = movieId
        self.user = user
        self.review = review
        self.rating = rating
        self.date = date

    def from_tuple(tuple):
        return Review(tuple[1], tuple[2], tuple[3], tuple[4], tuple[5], id = tuple[0])

    def __str__(self):
        return f"([{self.movieId}] {self.user}, {self.review}, {self.rating}, {self.date})"

    def __repr__(self):
        return f"([{self.movieId}] {self.user}, {self.review}, {self.rating}, {self.date})"

class Movie():
    def __init__(self, title, release_date, director, id = None):
        self.id = id
        self.title = title
        self.release_date = release_date
        self.director = director
    
    def from_tuple(tuple):
        return Movie(tuple[1], tuple[2], tuple[3], id = tuple[0])

    def __str__(self):
        return f"([{self.id}] {self.title}, {self.release_date}, {self.director})"

    def __repr__(self):
        return f"([{self.id}] {self.title}, {self.release_date}, {self.director})"
