# Fichier contenant les fonctions daccès à la bdd

from flaskext.mysql import MySQL
from data import *

class MovieQueries():
    def __init__(self, mysql: MySQL):
        self.mysql = mysql

    def get_movies(self):
        try:
            res = []
            conn = self.mysql.connect()
            cursor = conn.cursor()
            
            cursor.execute("SELECT * FROM movies_db")
            data = cursor.fetchall()

            for movieRaw in data:
                res.append(Movie.from_tuple(movieRaw))
            
            conn.commit()
            cursor.close()

            return res
        except Exception as e:
            return "Erreur get_movies : {}".format(e)

    def get_movie(self, id: int):
        try:
            res = []
            conn = self.mysql.connect()
            cursor = conn.cursor()
            
            cursor.execute(f"SELECT * FROM movies_db WHERE id = {id} ORDER BY title")
            data = cursor.fetchall()

            for movieRaw in data:
                res.append(Movie.from_tuple(movieRaw))
            
            conn.commit()
            cursor.close()

            return res
        except Exception as e:
            return "Erreur get_movie : {}".format(e)

    def get_reviews(self, movieId: int):
        try:
            res = []
            conn = self.mysql.connect()
            cursor = conn.cursor()
            
            cursor.execute(f"SELECT * FROM reviews_db WHERE movie_id = {movieId} ORDER BY date DESC")
            data = cursor.fetchall()

            for reviewRaw in data:
                res.append(Review.from_tuple(reviewRaw))
            
            conn.commit()
            cursor.close()

            return res
        except Exception as e:
            return "Erreur get_reviews : {}".format(e)

    def add_review(self, review: Review):
        try:
            conn = self.mysql.connect()
            cursor = conn.cursor()
            
            cursor.execute(f"INSERT INTO reviews_db (movie_id, user, review, rating, date) VALUES ({review.movieId},'{conn.escape_string(review.user)}','{conn.escape_string(review.review)}',{review.rating}, '{conn.escape_string(str(review.date))}')")
            
            conn.commit()
            cursor.close()

            return "ok"
        except Exception as e:
            return "Erreur add_review : {}".format(e)

    def add_movie(self, movie: Movie):
        try:
            conn = self.mysql.connect()
            cursor = conn.cursor()
              
            cursor.execute(f"INSERT INTO movies_db (title, release_date, director) VALUES ('{conn.escape_string(str(movie.title))}','{conn.escape_string(movie.release_date)}','{conn.escape_string(movie.director)}')")
            
            conn.commit()
            cursor.close()

            return "ok"
        except Exception as e:
            return "Erreur add_movie : {}".format(e)
    