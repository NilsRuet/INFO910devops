CREATE DATABASE IF NOT EXISTS `movies`;
USE `movies`;

DROP TABLE IF EXISTS `reviews_db`;
DROP TABLE IF EXISTS `movies_db`;

CREATE TABLE `movies_db` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`title` varchar(255),
	`release_date` DATETIME,
	`director` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `reviews_db` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`movie_id` INT NOT NULL,
	`user` varchar(255) NOT NULL,
	`review` varchar(4000) NOT NULL,
	`rating` FLOAT NOT NULL,
	`date` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `reviews_db` ADD CONSTRAINT `reviews_db_fk0` FOREIGN KEY (`movie_id`) REFERENCES `movies_db`(`id`);



